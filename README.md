# unfuck-scroll

For a brief overview and history of this program and the problems it tries to
solve, read https://cazander.ca/2022/fixing-borked-mouse/.

## Dependencies

 - interception-tools: https://gitlab.com/interception/linux/tools
 - a broken Razer Mamba wireless mouse

## Usage

    $ intercept -g /dev/input/event16 \
      | PYTHONUNBUFFERED=1 PYTHONOPTIMIZE= /usr/local/bin/unfuck-scroll --debounce-ms 500 \
      | uinput -d /dev/input/event16
